#!/usr/bin/env python3
# vim:ts=4:sts=4:sw=4:expandtab

import re
import socket
import ssl
import sys

STUDENT = sys.argv[1]
STUDENT.translate(str.maketrans('ąćęłńóśźżĄĆĘŁŃÓŚŹŻ', 'acelnoszzACELNOSZZ'))
assert re.match(r'[A-Z][a-z]+ [A-Z][a-z]+', STUDENT)
PORT = int(sys.argv[2])
assert (PORT > 2**10 and PORT < 2**16)
HOST = STUDENT.lower().replace(' ', '') + '.student.tcs.uj.edu.pl'

sock = socket.socket()
context = ssl.create_default_context()
sock = context.wrap_socket(sock, server_hostname=HOST)

print(HOST, PORT)
sock.connect((HOST, PORT))
sock.close()
